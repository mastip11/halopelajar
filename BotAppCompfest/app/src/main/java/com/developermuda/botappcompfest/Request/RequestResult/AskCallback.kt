package com.developermuda.botappcompfest.Request.RequestResult

import android.util.Log
import com.developermuda.botappcompfest.Data.WikiData
import org.json.JSONObject

/**
 * Created by HateLogcatError on 7/24/2017.
 */

interface AskCallback {

    fun setAskData(response: String) {
        val arrayWiki: ArrayList<WikiData> = ArrayList()

        val jsonData = JSONObject(response)
        if(jsonData.toString().contains("{\"query\":")) {
            (this as RequestError).onRequestErrorMessage("Maaf, kita tidak bisa menemukan keyword tersebut.")
        }
        else {
            val jsonArray = jsonData.getJSONArray("value")
            for (a in 0 .. jsonArray.length() - 1) {
                val valueData = jsonArray.getJSONObject(a)
                var thumbnail: String = "http://www.ecreativeim.com/blog/wp-content/uploads/2011/05/image-not-found.jpg"
                if(valueData.toString().contains("\"thumbnail\":")) {
                    thumbnail = valueData.getString("thumbnail")
                }
                val id = valueData.getString("pageid")
                val title = valueData.getString("title")
                val extract = valueData.getString("extract")
                val fullurl = valueData.getString("fullurl")

                Log.v("url", valueData.getString("fullurl"))

                arrayWiki.add(WikiData(id, title, extract, thumbnail, fullurl))
            }

            onWikiProcessSuccess(arrayWiki)
        }
    }

    fun onWikiProcessSuccess(dataWiki: ArrayList<WikiData>)
}