package com.developermuda.botappcompfest.Data

/**
 * Created by HateLogcatError on 7/22/2017.
 */

class NewsData(id: String, title: String, contains: String, image: String) {

    var id: String? = null
    var title: String? = null
    var image: String? = null
    var contains: String? = null

    init {
        this.id = id
        this.title = title
        this.contains = contains
        this.image = image
    }
}