package com.developermuda.botappcompfest.Custom

import android.content.Context
import android.support.v4.view.ViewPager
import android.util.AttributeSet
import android.view.MotionEvent

/**
 * Created by HateLogcatError on 7/19/2017.
 */

class CustomViewPager(context: Context, attrs: AttributeSet) : ViewPager(context, attrs) {

    val enabled: Boolean

    init {
        this.enabled = false
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (this.enabled) {
            return super.onTouchEvent(event)
        }

        return false
    }

    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
        if (this.enabled) {
            return super.onInterceptTouchEvent(event)
        }

        return false
    }

//    fun setPagingEnabled(enabled: Boolean) {
//        this.enabled = enabled
//    }
}
