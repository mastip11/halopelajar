package com.developermuda.botappcompfest.Request

import android.content.Context
import android.widget.Toast
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.developermuda.botappcompfest.Request.RequestResult.InfoSekolahCallback
import com.developermuda.botappcompfest.Request.RequestResult.LoginCallback
import com.developermuda.botappcompfest.Request.RequestResult.RequestError

/**
 * Created by HateLogcatError on 7/25/2017.
 */

class InfoSekolahRequest(context: Context, datas: ArrayList<String>) {

    val context: Context
    val datas: ArrayList<String>

    val URL_INFO_SEKOLAH: String = "http://halopelajar.anak-it.web.id/sekolah/"
    val METHOD_INFO_SEKOLAH: Int = Request.Method.POST

    init {
        this.context = context
        this.datas = datas
    }

    fun exec() {
        val requestQueue: RequestQueue = Volley.newRequestQueue(context)
        val stringRequest = object : StringRequest(METHOD_INFO_SEKOLAH, URL_INFO_SEKOLAH + datas[0], Response.Listener {
            response ->

            (context as InfoSekolahCallback).setSekolahData(response)

        }, Response.ErrorListener {
            error ->

            var errorMessage = ""
            if (error is TimeoutError) {
                errorMessage = "Timeout. Check your connection"
            } else if (error is NoConnectionError) {
                errorMessage = "Please turn on your connectivity"
            } else if (error is AuthFailureError) {
                errorMessage = "Authentication Error"
            } else if (error is ServerError) {
                errorMessage = "Server Error"
            } else if (error is NetworkError) {
                errorMessage = "Network Error"
            } else if (error is ParseError) {
                errorMessage = "Parse Error"
            }

            (context as RequestError).onRequestErrorMessage(errorMessage)
//            Toast.makeText(context, errorMessage, Toast.LENGTH_LONG).show()
        }) {
            override fun getParams(): MutableMap<String, String> {
                val data = HashMap<String, String>()
                data.put("name", datas[1])
                return data
            }

            override fun getHeaders(): MutableMap<String, String> {
                val map = HashMap<String, String>()
                map.put("Content-Type", "application/x-www-form-urlencoded")
                return map
            }
        }

        requestQueue.add(stringRequest)
    }
}