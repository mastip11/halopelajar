package com.developermuda.botappcompfest.Request.RequestResult

import com.developermuda.botappcompfest.Data.LoginData
import org.json.JSONObject

/**
 * Created by HateLogcatError on 7/23/2017.
 */
interface LoginCallback {

    fun loginData(data: String) {
        val isLoginSuccess: Boolean?

        val jsonObject = JSONObject(data)
        if(jsonObject.getInt("result_code") == 200) {
            val jsonArray = jsonObject.getJSONArray("value")
            val valueObject = jsonArray.getJSONObject(0)

            isLoginSuccess = true
            LoginData().email = valueObject.getString("email")
            LoginData().fullname = valueObject.getString("name")
            LoginData().token = valueObject.getString("remember_token")
        }
        else {
            isLoginSuccess = false
        }

        loginResult(isLoginSuccess)
    }

    fun loginResult(isLoginTrue: Boolean) {

    }
}