package com.developermuda.botappcompfest.Adapter

import android.content.Context
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.developermuda.botappcompfest.Data.ChatData
import com.developermuda.botappcompfest.R

/**
 * Created by HateLogcatError on 7/20/2017.
 */

class ChatListAdapter(context: Context, data: ArrayList<ChatData>): RecyclerView.Adapter<ChatListAdapter.Holder>() {

    var context: Context
    var data: ArrayList<ChatData>

    init {
        this.context = context
        this.data = data
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ChatListAdapter.Holder {
        val v: View
        val holder: Holder

        if (viewType == 2) {
            v = LayoutInflater.from(context).inflate(R.layout.chat_caroussel, null)
            holder = Holder(v, 2)
        }
        else {
            v = LayoutInflater.from(context).inflate(R.layout.chat_bubble, null)
            holder = Holder(v, 1)
        }

        return holder
    }

    override fun getItemViewType(position: Int): Int {
        var chatData: ChatData = data[position]

        if (chatData.chatType == "Berita") {
            return 2
        }
        else {
            return 1
        }
    }

    override fun onBindViewHolder(holder: ChatListAdapter.Holder?, position: Int) {
        var chatData: ChatData = data[position]

        holder!!.setIsRecyclable(false)

        if (chatData.isSenderIsMe == true) {
            holder.leftChatBubble.visibility = View.GONE
            holder.rightChatBubble.text = chatData.message
        }
        else {
            if (chatData.chatType.equals("Berita")) {
                if(chatData.dataNews!!.isNotEmpty()) {
                    val adapter: CarousselPagerAdapterNews = CarousselPagerAdapterNews(context, chatData.dataNews!!)
                    holder.leftChatCaroussel.adapter = adapter
                    holder.tabIndicator.setupWithViewPager(holder.leftChatCaroussel)
                }
                else if(chatData.dataWiki!!.isNotEmpty()) {
                    val adapter: CarousselPagerAdapterWiki = CarousselPagerAdapterWiki(context, chatData.dataWiki!!)
                    holder.leftChatCaroussel.adapter = adapter
                    holder.tabIndicator.setupWithViewPager(holder.leftChatCaroussel)
                }
            }
            else {
                holder.rightChatBubble.visibility = View.GONE
                holder.leftChatBubble.text = chatData.message
            }
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    class Holder(view: View, type: Int): RecyclerView.ViewHolder(view) {

        lateinit var leftChatBubble: TextView
        lateinit var rightChatBubble: TextView

        lateinit var leftChatCaroussel: ViewPager
        lateinit var tabIndicator: TabLayout

        init {
            if(type == 1) {
                leftChatBubble = view.findViewById(R.id.bubble_chat_left) as TextView
                rightChatBubble = view.findViewById(R.id.bubble_chat_right) as TextView
            }
            else {
                leftChatCaroussel = view.findViewById(R.id.viewPagerChatLeft) as ViewPager
                tabIndicator = view.findViewById(R.id.tabLayoutCaroussel) as TabLayout
            }
        }
    }
}