package com.developermuda.botappcompfest.Adapter

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.developermuda.botappcompfest.Data.NewsData
import com.developermuda.botappcompfest.Data.WikiData
import com.developermuda.botappcompfest.R
import com.squareup.picasso.Picasso

/**
 * Created by HateLogcatError on 7/23/2017.
 */

class CarousselPagerAdapterNews(context: Context, data: ArrayList<NewsData>) : PagerAdapter() {

    var context: Context
    var data: ArrayList<NewsData>

    init {
        this.context = context
        this.data = data
    }

    override fun instantiateItem(container: ViewGroup?, position: Int): Any {
        val v: View = LayoutInflater.from(context).inflate(R.layout.custom_caroussel, null)

        val newsData: NewsData = data[position]

        val thumbnail: ImageView = v.findViewById(R.id.imageThumbnail) as ImageView
        val titleText: TextView = v.findViewById(R.id.titleResponse) as TextView
        val extract: TextView = v.findViewById(R.id.textExtract) as TextView

        Log.v("ntap", newsData.image)

        titleText.text = newsData.title
        extract.text = newsData.contains
        Picasso.with(context).load(newsData.image).into(thumbnail)

        container!!.addView(v)

        return v
    }

    override fun getCount(): Int {
        return data.size
    }

    override fun isViewFromObject(view: View?, `object`: Any?): Boolean {
        return (view == `object` as FrameLayout)
    }

    override fun destroyItem(container: ViewGroup?, position: Int, `object`: Any?) {
        container!!.removeView(`object` as FrameLayout)
    }

}