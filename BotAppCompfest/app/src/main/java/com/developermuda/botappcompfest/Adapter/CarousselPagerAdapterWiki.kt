package com.developermuda.botappcompfest.Adapter

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.developermuda.botappcompfest.Data.ChatData
import com.developermuda.botappcompfest.Data.WikiData
import com.developermuda.botappcompfest.R
import com.squareup.picasso.Picasso

/**
 * Created by HateLogcatError on 7/21/2017.
 */

class CarousselPagerAdapterWiki(context: Context, data: ArrayList<WikiData>): PagerAdapter() {

    var context: Context
    var data: ArrayList<WikiData>

    init {
        this.context = context
        this.data = data
    }

    override fun instantiateItem(container: ViewGroup?, position: Int): Any {
        val v: View = LayoutInflater.from(context).inflate(R.layout.custom_caroussel, null)

        val wikiData: WikiData = data[position]

        val layout: RelativeLayout = v.findViewById(R.id.layout_custom_caroussel) as RelativeLayout
        val thumbnail: ImageView = v.findViewById(R.id.imageThumbnail) as ImageView
        val titleText: TextView = v.findViewById(R.id.titleResponse) as TextView
        val extract: TextView = v.findViewById(R.id.textExtract) as TextView

        layout.setOnClickListener {
            if(wikiData.fullurl != null || wikiData.fullurl != "") {
                val intent: Intent = Intent(Intent.ACTION_VIEW)
                intent.setData(Uri.parse(wikiData.fullurl))
                val intent1 = Intent.createChooser(intent, "Pilih browser")
                context.startActivity(intent1)
            }
        }
        titleText.text = wikiData.title
        extract.text = wikiData.extract
        Picasso.with(context).load(wikiData.thumbnail).into(thumbnail)

        container!!.addView(v)

        return v
    }

    override fun getCount(): Int {
        return data.size
    }

    override fun isViewFromObject(view: View?, `object`: Any?): Boolean {
        return (view == `object` as FrameLayout)
    }

    override fun destroyItem(container: ViewGroup?, position: Int, `object`: Any?) {
        container!!.removeView(`object` as FrameLayout)
    }

}