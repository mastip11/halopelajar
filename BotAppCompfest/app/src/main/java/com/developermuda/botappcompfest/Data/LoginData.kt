package com.developermuda.botappcompfest.Data

/**
 * Created by HateLogcatError on 7/22/2017.
 */

class LoginData {

    var email: String? = null
    var fullname: String? = null
    var token: String? = null
}