package com.developermuda.botappcompfest

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.EditText
import android.widget.Toast
import com.developermuda.botappcompfest.Adapter.ChatListAdapter
import com.developermuda.botappcompfest.Data.ChatData
import com.developermuda.botappcompfest.Data.NewsData
import com.developermuda.botappcompfest.Data.WikiData
import com.developermuda.botappcompfest.Core.ChatEngine
import com.developermuda.botappcompfest.Data.SekolahData
import com.developermuda.botappcompfest.Request.RequestResult.AskCallback
import com.developermuda.botappcompfest.Request.RequestResult.InfoSekolahCallback
import com.developermuda.botappcompfest.Request.RequestResult.NewsCallback
import com.developermuda.botappcompfest.Request.RequestResult.RequestError
import uk.co.chrisjenx.calligraphy.CalligraphyConfig
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper

class ChatActivity : AppCompatActivity(), NewsCallback , AskCallback, InfoSekolahCallback, RequestError{

    lateinit var etMessageChat: EditText

    lateinit var listChat: RecyclerView
    lateinit var adapter: ChatListAdapter

    lateinit var rie: ChatEngine

    var arrayChatData: ArrayList<ChatData> = ArrayList()
    var wikiData: ArrayList<WikiData> = ArrayList()
    var arrayNewsData: ArrayList<NewsData> = ArrayList()

    var exitCounter: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)

        initCalligraphy()

        initChat()
        listChat = findViewById(R.id.chatList) as RecyclerView

        initAdapter()

        etMessageChat = findViewById(R.id.etMessageChat) as EditText
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

    private fun initCalligraphy() {
        CalligraphyConfig.initDefault(CalligraphyConfig.Builder().setFontAttrId(R.attr.fontPath).setDefaultFontPath("fonts/Poppins-Light.ttf").build())
    }

    fun initAdapter() {
        adapter = ChatListAdapter(this, arrayChatData)

        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this)
        listChat.layoutManager = layoutManager
        listChat.itemAnimator = DefaultItemAnimator()
        listChat.adapter = adapter
    }

    private fun initChat() {
        rie = ChatEngine(this)
        arrayChatData.add(rie.initChat())
    }

    fun sendingChatMessage(view: View) {
        if(validation()) {
            val message: String = getChatMessage()
            userSendingChat(message)
            clearChatText()

            val askedThing = rie.chatChecker(message)

            if(askedThing != "BERITA" || askedThing != "INFOSEKOLAH") {
                processChat(askedThing)
            }
        }
        else {
            Toast.makeText(this, "Mohon isikan data", Toast.LENGTH_LONG).show()
        }
    }

    private fun validation(): Boolean {
        if(etMessageChat.text.isEmpty()) {
            return false
        }
        else {
            return true
        }
    }

    private fun getChatMessage(): String {
        return etMessageChat.text.toString()
    }

    private fun processChat(askedThing: String) {
        showDataToChat(askedThing, false, "Chat", wikiData, arrayNewsData)
    }

    private fun userSendingChat(chat: String) {
        showDataToChat(chat, true, "Chat", wikiData, arrayNewsData)
    }

    private fun clearChatText() {
        etMessageChat.setText("")
    }

    override fun finishRequest(data: ArrayList<NewsData>) {
        showDataToChat("", false, "Berita", wikiData, data)
    }

    override fun onWikiProcessSuccess(dataWiki: ArrayList<WikiData>) {
        showDataToChat("", false, "Berita", dataWiki, arrayNewsData)
    }

    override fun onSekolahDataSuccess(sekolahData: ArrayList<SekolahData>) {
        if(sekolahData.size > 0) {
            val dataSekolah: SekolahData = sekolahData[0]
            val id = dataSekolah.id
            val school = dataSekolah.schoolName
            val address = dataSekolah.address
            val region = dataSekolah.region
            val phone = dataSekolah.phone

            val message = "Berikut adalah data sekolah yang kamu minta : \nNama Sekolah : $school\nAlamat : $address\nRegion : $region\nNo Telepon : $phone"

            showDataToChat(message, false, "Chat", wikiData, arrayNewsData)
        }
        else {
            showDataToChat("Sekolah yang kamu minta tidak terdaftar dalam database kami.", false, "Chat", wikiData, arrayNewsData)
        }

    }

    override fun onRequestErrorMessage(message: String) {
        showDataToChat(message, false, "Chat", wikiData, arrayNewsData)
    }

    override fun onBackPressed() {
        Toast.makeText(this, "Tekan dua kali untuk keluar", Toast.LENGTH_LONG).show()
        exitCounter++
        val thread: Thread = Thread(Runnable {
            run {
                Thread.sleep(1000)
                exitCounter = 0
            }
        })
        thread.start()
    }

    fun showDataToChat(message: String, isSenderIsUser: Boolean, typeMessage: String, wikiData: ArrayList<WikiData>, newsData: ArrayList<NewsData>) {
        arrayChatData.add(ChatData(arrayChatData.size + 1, message, isSenderIsUser, typeMessage, wikiData, newsData))
        adapter.notifyDataSetChanged()
        listChat.smoothScrollToPosition(arrayChatData.size - 1)
    }
}
