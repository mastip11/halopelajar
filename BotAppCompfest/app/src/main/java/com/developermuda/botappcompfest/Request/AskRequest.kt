package com.developermuda.botappcompfest.Request

import android.content.Context
import android.widget.Toast
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.developermuda.botappcompfest.Request.RequestResult.AskCallback
import com.developermuda.botappcompfest.Request.RequestResult.RequestError

/**
 * Created by HateLogcatError on 7/24/2017.
 */

class AskRequest(context: Context, keyword: String) {

    val ASK_URL = "http://halopelajar.anak-it.web.id/wikipedia"
    val ASK_METHOD = Request.Method.POST

    val context: Context

    val keyword: String

    init {
        this.context = context
        this.keyword = keyword
    }

    fun exec() {
        val requestQueue = Volley.newRequestQueue(context)
        val stringRequest = object : StringRequest(ASK_METHOD, ASK_URL, Response.Listener {
            response ->

            (context as AskCallback).setAskData(response)
        }, Response.ErrorListener {
            error ->
            var errorMessage = ""
            if (error is TimeoutError) {
                errorMessage = "Timeout. Check your connection"
            } else if (error is NoConnectionError) {
                errorMessage = "Please turn on your connectivity"
            } else if (error is AuthFailureError) {
                errorMessage = "Authentication Error"
            } else if (error is ServerError) {
                errorMessage = "Server Error"
            } else if (error is NetworkError) {
                errorMessage = "Network Error"
            } else if (error is ParseError) {
                errorMessage = "Parse Error"
            }

            (context as RequestError).onRequestErrorMessage(errorMessage)

//            Toast.makeText(context, errorMessage, Toast.LENGTH_LONG).show()
        }) {
            override fun getParams(): MutableMap<String, String> {
                val data = HashMap<String, String>()
                data.put("keyword", keyword)
                return data
            }
        }

        requestQueue.add(stringRequest)
    }
}