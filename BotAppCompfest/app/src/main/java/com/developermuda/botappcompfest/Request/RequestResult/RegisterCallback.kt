package com.developermuda.botappcompfest.Request.RequestResult

import org.json.JSONObject

/**
 * Created by HateLogcatError on 7/24/2017.
 */
interface RegisterCallback {

    fun processRegister(data: String) {
        val jsonData = JSONObject(data)

        if(jsonData.getInt("result_code") == 200) {
            registerResult(1)
        }
        else {
            registerResult(2)
        }
    }

    fun registerResult(code: Int)
}