package com.developermuda.botappcompfest.Request

import android.content.Context
import android.widget.Toast
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.developermuda.botappcompfest.Data.WikiData
import com.developermuda.botappcompfest.Request.RequestResult.NewsCallback
import com.developermuda.botappcompfest.Request.RequestResult.RequestError

/**
 * Created by HateLogcatError on 7/22/2017.
 */

class NewsRequest(context: Context){

    val URL_NEWS = "http://halopelajar.anak-it.web.id/news/"
    val METHOD_NEWS = Request.Method.GET

    val context: Context

    init {
        this.context = context
    }

    fun exec() {
        val requestQueue: RequestQueue = Volley.newRequestQueue(context)
        val stringRequest = object : StringRequest(METHOD_NEWS, URL_NEWS, Response.Listener {
            response ->

            (context as NewsCallback).setNewsData(response)

        }, Response.ErrorListener { 
            error ->

            var errorMessage = ""
            if (error is TimeoutError) {
                errorMessage = "Timeout. Check your connection"
            } else if (error is NoConnectionError) {
                errorMessage = "Please turn on your connectivity"
            } else if (error is AuthFailureError) {
                errorMessage = "Authentication Error"
            } else if (error is ServerError) {
                errorMessage = "Server Error"
            } else if (error is NetworkError) {
                errorMessage = "Network Error"
            } else if (error is ParseError) {
                errorMessage = "Parse Error"
            }

            (context as RequestError).onRequestErrorMessage(errorMessage)
//            Toast.makeText(context, errorMessage, Toast.LENGTH_LONG).show()
        }) {

        }

        requestQueue.add(stringRequest)
    }
}