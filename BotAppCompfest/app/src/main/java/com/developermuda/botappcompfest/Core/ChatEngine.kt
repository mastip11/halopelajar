package com.developermuda.botappcompfest.Core

import android.content.Context
import android.util.Log
import com.developermuda.botappcompfest.Data.ChatData
import com.developermuda.botappcompfest.Data.LoginData
import com.developermuda.botappcompfest.Data.NewsData
import com.developermuda.botappcompfest.Data.WikiData
import com.developermuda.botappcompfest.Request.AskRequest
import com.developermuda.botappcompfest.Request.InfoSekolahRequest
import com.developermuda.botappcompfest.Request.NewsRequest

/**
 * Created by HateLogcatError on 7/22/2017.
 */

class ChatEngine {

    lateinit var context: Context

    var datas: ArrayList<WikiData> = ArrayList()
    var datanews: ArrayList<NewsData> = ArrayList()

    val firstChatData: ChatData = ChatData(1, "Hallo! Namaku Rie. Aku akan membantumu dalam belajar. Mantap pisan euy! Cukup tulis dengan format seperti " +
            "\n\n'Saya ingin bertanya tentang rumus lingkaran dong'\n\n" +
            "Atau dengan langsung menuliskan keyword yang ingin kamu cari. Selamat belajar!\n\n" +
            "Ohya, jika kamu kebingungan, keyword /help mungkin bisa membantumu.", false, "Chat", datas, datanews)

    val MESSAGE_HELP = "Cukup tulis dengan format seperti" +
            "\n\n'Saya ingin bertanya tentang rumus lingkaran dong'\n\n" +
            "Atau dengan langsung menuliskan keyword yang ingin kamu cari." +
            "\n\nList keyword : \n\n" +
            "/help : Digunakan untuk memunculkan bantuan\n" +
            "/infosekolah (jenjang) (nomor sekolah) : Digunakan untuk bertanya mengenai info sekolah\n"+
            "/hotnews : Digunakan untuk mendapatkan berita terbaru tentang pendidikan"

    val MESSAGE_TANYA_SEKOLAH = "Permintaan data sekolah sedang diproses"
    val MESSAGE_BERITA_TERBARU = "Permintaan berita pendidikan sedang diproses"
    val MESSAGE_NOT_FOUND = "Mohon tuliskan menu yang benar sesuai dengan keyword yang tersedia. Jika kamu bingung, tekan --help"

    val ASK_DELIMITER = arrayOf<String>("nanya", "tentang", "nyari", "cari", "tanya", "bertanya")
    val IGNORE_DELIMITER = arrayOf<String>("dong", "nih", "euy")

    constructor(context: Context) {
        this.context = context
    }

    fun initChat(): ChatData {
        return firstChatData
    }

    fun chatChecker(message: String): String {
        if(message.toLowerCase().contains("hallo rie") || message.toLowerCase().contains("hai rie")) {
            return "Hallo " + LoginData().fullname + " , ada yang bisa di bantu? Kalau kamu kebingungan, bisa menggunakan keyword /help"
        }
        else if(message.substring(0,1) == "/") {
            if(message.toLowerCase().equals("/help")) {
                return MESSAGE_HELP
            }
            else if(message.contains("/infosekolah")) {
                InfoSekolahRequest(context, getSchoolName(message)).exec()
                return MESSAGE_TANYA_SEKOLAH
            }
            else if(message.toLowerCase().equals("/hotnews")) {
                NewsRequest(context).exec()
                return MESSAGE_BERITA_TERBARU
            }
            else {
                return MESSAGE_NOT_FOUND
            }
        }
        else {
            val msg = message.toLowerCase().split(' ')
            var wordSplitter: Int = 0

            for (a in 0 .. msg.size - 1) {
                for (b in 0 .. ASK_DELIMITER.size - 1) {
                    if(msg[a].contains(ASK_DELIMITER[b])) {
                        wordSplitter = a + 1
                        break
                    }
                }
            }

            if (wordSplitter == 0) {
//                return getAskedThing(msg)
                AskRequest(context, getAskedThing(msg)).exec()
                Log.v("something", getAskedThing(msg))
                return "Permintaan " + getAskedThing(msg) + " sedang diproses"
            }
            else {
                AskRequest(context, getAskedThing(wordSplitter, msg)).exec()
//                return getAskedThing(wordSplitter, msg)
                return "Permintaan " + getAskedThing(wordSplitter, msg) + " sedang diproses"
            }
        }
    }

    private fun getAskedThing(splitter: Int, msg: List<String>): String {
        var askedThing: String = ""

        for (a in splitter .. msg.size - 1) {
            for (b in 0 .. IGNORE_DELIMITER.size - 1) {
                if (!msg[a].contains(IGNORE_DELIMITER[b])) {
                    if(a == splitter) {
                        askedThing += msg[a]
                    }
                    else {
                        askedThing += " " + msg[a]
                    }

                    break
                }
                else {
                    // do nothing
                    break
                }
            }
        }

        return askedThing
    }

    private fun getAskedThing(msg: List<String>): String {
        var askedThing: String = ""

        for (a in 0 .. msg.size - 1) {
            for (b in 0 .. IGNORE_DELIMITER.size - 1) {
                if (msg[a] != IGNORE_DELIMITER[b]) {
                    if(a == 0) {
                        askedThing += msg[a]
                    }
                    else {
                        askedThing += " " + msg[a]
                    }
                    break
                }
                else {
                    // do nothing
                    break
                }
            }
        }

        return askedThing
    }

    private fun getSchoolName(sch: String): ArrayList<String> {
        val datas: ArrayList<String> = ArrayList()

        val dataSchool = sch.toLowerCase().split(' ')

        val schGrade = dataSchool[1]
        val schNum = dataSchool[2]

        datas.add(schGrade)
        datas.add(schNum)

        return datas
    }
}