package com.developermuda.botappcompfest.Data

/**
 * Created by HateLogcatError on 7/20/2017.
 */

class ChatData(id: Int, message: String, isSenderIsMe: Boolean, chatType: String, data: ArrayList<WikiData>, dataNews: ArrayList<NewsData>) {

    var id: Int? = null
    var message: String? = null
    var isSenderIsMe: Boolean? = null
    var chatType: String? = null
    var dataWiki: ArrayList<WikiData>? = null
    var dataNews: ArrayList<NewsData>? = null
    var time: String? = null

    init {
        this.id = id
        this.message = message
        this.isSenderIsMe = isSenderIsMe
        this.chatType = chatType
        this.dataWiki = data
        this.dataNews = dataNews
    }
}
