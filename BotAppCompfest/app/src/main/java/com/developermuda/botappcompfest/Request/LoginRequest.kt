package com.developermuda.botappcompfest.Request

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.widget.Toast
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.developermuda.botappcompfest.ChatActivity
import com.developermuda.botappcompfest.Data.LoginData
import com.developermuda.botappcompfest.LoginActivity
import com.developermuda.botappcompfest.Request.RequestResult.LoginCallback
import org.json.JSONObject
import kotlin.collections.HashMap

/**
 * Created by HateLogcatError on 7/22/2017.
 */

class LoginRequest(context: Context, email: String, password: String) {

    val context: Context
    val URL_LOGIN: String = "http://halopelajar.anak-it.web.id/login"
    val METHOD_LOGIN: Int = Request.Method.POST

    var email: String
    var password: String

    init {
        this.context = context
        this.email = email
        this.password = password
    }

    fun exec() {
        val progressDialog = ProgressDialog.show(context, "Please wait", "Please wait while login data", true, false)

        val requestQueue: RequestQueue = Volley.newRequestQueue(context)
        val stringRequest = object : StringRequest(METHOD_LOGIN, URL_LOGIN, Response.Listener {
            response ->

            (context as LoginCallback).loginData(response)

            progressDialog.dismiss()

        }, Response.ErrorListener {
            error ->

            var errorMessage = ""
            if (error is TimeoutError) {
                errorMessage = "Timeout. Check your connection"
            } else if (error is NoConnectionError) {
                errorMessage = "Please turn on your connectivity"
            } else if (error is AuthFailureError) {
                errorMessage = "Authentication Error"
            } else if (error is ServerError) {
                errorMessage = "Server Error"
            } else if (error is NetworkError) {
                errorMessage = "Network Error"
            } else if (error is ParseError) {
                errorMessage = "Parse Error"
            }

            progressDialog.dismiss()

            Toast.makeText(context, errorMessage, Toast.LENGTH_LONG).show()
        }) {
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val map = HashMap<String, String>()
                map.put("email", email)
                map.put("password", password)
                return map
            }

            override fun getHeaders(): MutableMap<String, String> {
                val map = HashMap<String, String>()
                map.put("Content-Type", "application/x-www-form-urlencoded")
                return map
            }
        }

        requestQueue.add(stringRequest)
    }
}