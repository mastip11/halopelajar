package com.developermuda.botappcompfest.Data

/**
 * Created by HateLogcatError on 7/25/2017.
 */

class SekolahData(id: String, schoolName: String, address: String, region: String, phone: String) {

    var id: String? = null
    var schoolName: String? = null
    var address: String? = null
    var region: String? = null
    var phone: String? = null

    init {
        this.id = id
        this.schoolName = schoolName
        this.address = address
        this.region = region
        this.phone = phone
    }
}