package com.developermuda.botappcompfest.Login


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.developermuda.botappcompfest.*
import com.developermuda.botappcompfest.Request.LoginRequest
import com.developermuda.botappcompfest.Request.RequestResult.LoginCallback

/**
 * A simple [Fragment] subclass.
 */
class LoginFragment : Fragment() {

    lateinit var buttonLogin: Button
    lateinit var etUsername: EditText
    lateinit var etPassword: EditText

    var username: String = ""
    var password: String = ""

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val v = inflater!!.inflate(R.layout.fragment_login, container, false)

        buttonLogin = v.findViewById(R.id.buttonLogin) as Button
        etUsername = v.findViewById(R.id.etUsernameLogin) as EditText
        etPassword = v.findViewById(R.id.etPasswordLogin) as EditText

        buttonLogin.setOnClickListener {
            getData()
            if(validation()) {
                LoginRequest(context, username, password).exec()
            }
            else {
                Toast.makeText(context, "Mohon isikan data", Toast.LENGTH_LONG).show()
            }
        }
        return v
    }

    fun getData() {
        this.username = etUsername.text.toString()
        this.password = etPassword.text.toString()
    }

    fun validation(): Boolean {
        if(username.isEmpty() || password.isEmpty()) {
            return false
        }
        else {
            return true
        }
    }
}