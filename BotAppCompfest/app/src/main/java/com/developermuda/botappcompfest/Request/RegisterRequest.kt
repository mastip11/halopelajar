package com.developermuda.botappcompfest.Request

import android.app.ProgressDialog
import android.content.Context
import android.widget.Toast
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.developermuda.botappcompfest.Request.RequestResult.RegisterCallback

/**
 * Created by HateLogcatError on 7/24/2017.
 */

class RegisterRequest(context: Context, email: String, fullname: String, password: String, cPassword: String) {

    val REGISTER_URL = "http://halopelajar.anak-it.web.id/register"
    val REGISTER_METHOD = Request.Method.POST

    var context: Context

    val email: String
    val fullname: String
    val password: String
    val cPassword: String

    init {
        this.context = context
        this.email = email
        this.fullname = fullname
        this.password = password
        this.cPassword = cPassword
    }

    fun exec() {
        val progressDialog = ProgressDialog.show(context, "Please wait", "Please wait while register data", true, false)

        val requestQueue: RequestQueue = Volley.newRequestQueue(context)
        val stringRequest = object: StringRequest(REGISTER_METHOD, REGISTER_URL, Response.Listener {
            response ->
            (context as RegisterCallback).processRegister(response)

            progressDialog.dismiss()

        }, Response.ErrorListener {
            error ->

            var errorMessage = ""
            if (error is TimeoutError) {
                errorMessage = "Timeout. Check your connection"
            } else if (error is NoConnectionError) {
                errorMessage = "Please turn on your connectivity"
            } else if (error is AuthFailureError) {
                errorMessage = "Authentication Error"
            } else if (error is ServerError) {
                errorMessage = "Server Error"
            } else if (error is NetworkError) {
                errorMessage = "Network Error"
            } else if (error is ParseError) {
                errorMessage = "Parse Error"
            }

            progressDialog.dismiss()

            Toast.makeText(context, errorMessage, Toast.LENGTH_LONG).show()
        }) {
            override fun getParams(): MutableMap<String, String> {
                val data = HashMap<String, String>()
                data.put("email", email)
                data.put("name", fullname)
                data.put("password", password)
                data.put("password_confirmation", cPassword)
                return data
            }
        }

        requestQueue.add(stringRequest)
    }
}
