package com.developermuda.botappcompfest

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.ActionBar
import uk.co.chrisjenx.calligraphy.CalligraphyConfig
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initCalligraphy()
        hideActionBar()
        goToNextActivity()
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

    private fun initCalligraphy() {
        CalligraphyConfig.initDefault(CalligraphyConfig.Builder().setFontAttrId(R.attr.fontPath).setDefaultFontPath("fonts/Poppins-Light.ttf").build())
    }

    private fun goToNextActivity() {
        val thread: Thread = Thread(Runnable {
            run {
                Thread.sleep(1000)
                intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
                finish()
            }
        })
        thread.start()
    }

    private fun hideActionBar() {
        var actionBar: ActionBar = supportActionBar!!
        actionBar.hide()
    }
}
