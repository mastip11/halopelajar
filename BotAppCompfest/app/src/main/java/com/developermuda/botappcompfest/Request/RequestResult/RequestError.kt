package com.developermuda.botappcompfest.Request.RequestResult

/**
 * Created by HateLogcatError on 7/25/2017.
 */
interface RequestError {

    fun onRequestErrorMessage(message: String)
}