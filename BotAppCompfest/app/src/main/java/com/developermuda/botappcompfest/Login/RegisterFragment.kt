package com.developermuda.botappcompfest.Login


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.developermuda.botappcompfest.R
import com.developermuda.botappcompfest.Request.RegisterRequest
import com.developermuda.botappcompfest.Request.RequestResult.RegisterCallback


/**
 * A simple [Fragment] subclass.
 */
class RegisterFragment : Fragment() {

    lateinit var etEmail: EditText
    lateinit var etFullname: EditText
    lateinit var etPassword: EditText
    lateinit var etCPassword: EditText
    lateinit var buttonRegister: Button

    lateinit var email: String
    lateinit var fullname: String
    lateinit var password: String
    lateinit var cpassword: String

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val v: View = inflater!!.inflate(R.layout.fragment_register, container, false)

        etEmail = v.findViewById(R.id.etUsernameRegister) as EditText
        etFullname = v.findViewById(R.id.etFullnameRegister) as EditText
        etPassword = v.findViewById(R.id.etPasswordRegister) as EditText
        etCPassword = v.findViewById(R.id.etCPasswordRegister) as EditText
        buttonRegister = v.findViewById(R.id.buttonRegister) as Button

        buttonRegister.setOnClickListener {
            getData()

            RegisterRequest(context, email, fullname, password, cpassword).exec()
        }

        return v
    }

    private fun getData() {
        this.email = etEmail.text.toString()
        this.fullname = etFullname.text.toString()
        this.password = etPassword.text.toString()
        this.cpassword = etCPassword.text.toString()
    }

}
