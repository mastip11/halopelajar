package com.developermuda.botappcompfest.Data

/**
 * Created by HateLogcatError on 7/21/2017.
 */

class WikiData(pageID: String, title: String, extract: String, thumbnail: String, fullurl: String) {

    var pageID: String? = null
    var title: String? = null
    var extract: String? = null
    var fullurl: String? = null
    var thumbnail: String? = null

    init {
        this.pageID = pageID
        this.title = title
        this.extract = extract
        this.fullurl = fullurl
        this.thumbnail = thumbnail
    }
}