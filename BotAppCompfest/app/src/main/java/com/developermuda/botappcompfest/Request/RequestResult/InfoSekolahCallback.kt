package com.developermuda.botappcompfest.Request.RequestResult

import com.developermuda.botappcompfest.Data.SekolahData
import org.json.JSONObject

/**
 * Created by HateLogcatError on 7/25/2017.
 */
interface InfoSekolahCallback {

    fun setSekolahData(data: String) {
        val dataSekolah: ArrayList<SekolahData> = ArrayList()

        val jsonData = JSONObject(data)
        if(jsonData.getInt("result_code") == 200) {
            val arrayValue = jsonData.getJSONArray("value")
            if(arrayValue.length() > 0) {
                val dataValue = arrayValue.getJSONObject(0)

                val id = dataValue.getString("id")
                val school = dataValue.getString("school")
                val address = dataValue.getString("address")
                val region = dataValue.getString("region")
                val phone = dataValue.getString("phone")

                dataSekolah.add(SekolahData(id, school, address, region, phone))
            }
        }

        onSekolahDataSuccess(dataSekolah)
    }

    fun onSekolahDataSuccess(sekolahData: ArrayList<SekolahData>)
}