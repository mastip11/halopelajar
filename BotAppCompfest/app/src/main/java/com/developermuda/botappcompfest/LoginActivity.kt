package com.developermuda.botappcompfest

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.view.View
import android.widget.Toast
import com.developermuda.botappcompfest.Custom.CustomViewPager
import com.developermuda.botappcompfest.Login.LoginFragment
import com.developermuda.botappcompfest.Login.RegisterFragment
import com.developermuda.botappcompfest.Request.RequestResult.LoginCallback
import com.developermuda.botappcompfest.Request.RequestResult.RegisterCallback
import uk.co.chrisjenx.calligraphy.CalligraphyConfig
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper

class LoginActivity : AppCompatActivity(), LoginCallback, RegisterCallback {

    lateinit var viewPager: CustomViewPager
    var exitCounter: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        initCalligraphy()

        intent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY

        viewPager = findViewById(R.id.pagerLogin) as CustomViewPager

        viewPager!!.adapter = CustomPagerAdapter(supportFragmentManager)
        viewPager!!.currentItem = 0
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

    private fun initCalligraphy() {
        CalligraphyConfig.initDefault(CalligraphyConfig.Builder().setFontAttrId(R.attr.fontPath).setDefaultFontPath("fonts/Poppins-Light.ttf").build())
    }

    fun registerClicked(view: View) {
        viewPager.currentItem = 1
    }

    override fun onBackPressed() {
        if (viewPager.currentItem == 1) {
            viewPager.currentItem = 0
        }
        else {
            Toast.makeText(this, "Tekan dua kali untuk keluar", Toast.LENGTH_LONG).show()
            exitCounter++

            if(exitCounter == 2) {
                super.onBackPressed()
            }
            else {
                var thread: Thread = Thread(Runnable {
                    run {
                        Thread.sleep(1000)
                        exitCounter = 0
                    }
                })
                thread.start()
            }
        }
    }

    override fun loginResult(isLoginTrue: Boolean) {
        if(isLoginTrue) {
            val intent: Intent = Intent(this, ChatActivity::class.java)
            startActivity(intent)
            finish()
        }
        else {
            Toast.makeText(this, "Email / password kamu salah. Mohon cek kembali", Toast.LENGTH_LONG).show()
        }
    }

    override fun registerResult(code: Int) {
        if(code == 1) {
            Toast.makeText(this, "Register Berhasil", Toast.LENGTH_LONG).show()
            viewPager.currentItem = 0
        }
        else {
            Toast.makeText(this, "Register Gagal", Toast.LENGTH_LONG).show()
        }
    }

    class CustomPagerAdapter(fm: FragmentManager): FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            when(position) {
                0 -> {
                    var login: LoginFragment = LoginFragment()
                    return login
                }
                1 -> {
                    var register: RegisterFragment = RegisterFragment()
                    return register
                }
            }
            return null!!
        }

        override fun getCount(): Int {
            return 2
        }
    }
}
