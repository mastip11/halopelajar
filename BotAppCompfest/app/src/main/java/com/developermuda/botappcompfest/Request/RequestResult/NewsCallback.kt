package com.developermuda.botappcompfest.Request.RequestResult

import com.developermuda.botappcompfest.Data.NewsData
import org.json.JSONObject

/**
 * Created by HateLogcatError on 7/23/2017.
 */
interface NewsCallback {

    fun setNewsData(response: String) {
        var dataNews: ArrayList<NewsData> = ArrayList()

        val jsonObject = JSONObject(response)
        if(jsonObject.getInt("result_code") == 200) {
            val jsonArray = jsonObject.getJSONArray("values")

            for(a in 0 .. jsonArray.length() - 1) {
                val objectValue = jsonArray.getJSONObject(a)

                val id: String = objectValue.getString("id")
                val title: String = objectValue.getString("title")
                val contains: String = objectValue.getString("contains")
                val image: String = objectValue.getString("url")

                dataNews.add(NewsData(id, title, contains, image))
            }
        }

        finishRequest(dataNews)
    }

    fun finishRequest(data: ArrayList<NewsData>)
}